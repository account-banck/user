package com.example.usermanagement.domain.mapper;

import java.util.Collection;
import java.util.List;

public interface GenericJpaMapper<D, T> {
    T toJpa(D source);

    D toDomain(T domain);

    List<T> toJpa(Collection<D> sources);

    List<D> toDomain(Collection<T> domains);
}
