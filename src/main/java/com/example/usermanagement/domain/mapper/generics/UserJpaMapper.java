package com.example.usermanagement.domain.mapper.generics;

import com.example.usermanagement.domain.mapper.GenericJpaMapper;
import com.example.usermanagement.domain.model.User;
import com.example.usermanagement.infrastructure.persistence.entity.UserJpa;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserJpaMapper extends GenericJpaMapper<User, UserJpa> {
}