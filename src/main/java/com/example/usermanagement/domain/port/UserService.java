
package com.example.usermanagement.domain.port;

import com.example.usermanagement.domain.model.CustomTokenResponse;
import com.example.usermanagement.domain.model.User;

public interface UserService {
    User registerUser(User user);

    CustomTokenResponse loginUser(String username, String password);

    void logoutUser(String userId);
}
