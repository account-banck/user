package com.example.usermanagement.domain.model;

import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class CustomTokenResponse {
    private BearerAccessToken bearerAccessToken;
    private RefreshToken refreshToken;
    private String accessToken;
    private String idToken;

}
