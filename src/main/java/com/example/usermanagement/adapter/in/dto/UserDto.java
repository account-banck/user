package com.example.usermanagement.adapter.in.dto;

public record UserDto(Long id, String username, String password) {
}

