package com.example.usermanagement.adapter.in.mapper;

import java.util.Collection;
import java.util.List;

public interface GenericDtoMapper<D, T> {
    T toDto(D source);

    D toDomain(T dto);

    List<T> toDto(Collection<D> sources);

    List<D> toDomain(Collection<T> dtos);
}
