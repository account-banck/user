
package com.example.usermanagement.adapter.in.api;

import com.example.usermanagement.adapter.in.dto.UserDto;
import com.example.usermanagement.adapter.in.mapper.generics.UserDtoMapper;
import com.example.usermanagement.common.MapperService;
import com.example.usermanagement.domain.model.CustomTokenResponse;
import com.example.usermanagement.domain.model.User;
import com.example.usermanagement.domain.port.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private MapperService mapperService;


    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseEntity<UserDto> registerUser(@RequestBody UserDto user) {
        var mapper = mapperService.getByType(UserDtoMapper.class);
        User registeredUser = userService.registerUser(mapper.toDomain(user));
        return ResponseEntity.ok(mapper.toDto(registeredUser));
    }

    @PostMapping("/login")
    public ResponseEntity<Object> loginUser(@RequestBody User user) {
        CustomTokenResponse loggedInUser = userService.loginUser(user.getUsername(), user.getPassword());
        if (loggedInUser == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(loggedInUser);

    }


    @PostMapping("/logout")
    public ResponseEntity<Void> logoutUser(@RequestParam String userId) {
        userService.logoutUser(userId);
        return ResponseEntity.ok().build();
    }
}
