package com.example.usermanagement.adapter.in.mapper.generics;

import com.example.usermanagement.adapter.in.dto.UserDto;
import com.example.usermanagement.adapter.in.mapper.GenericDtoMapper;
import com.example.usermanagement.domain.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDtoMapper extends GenericDtoMapper<User, UserDto> {
}
