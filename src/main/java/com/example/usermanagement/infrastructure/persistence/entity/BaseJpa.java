package com.example.usermanagement.infrastructure.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.Getter;
import lombok.Setter;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseJpa {
  private static final String TIME_ZONE = "Europe/Paris";

  @Column(nullable = false)
  protected Instant updated;

  @Column(nullable = false, updatable = false)
  protected Instant created;

  @PrePersist
  protected void onCreate() {
    var clock = Clock.systemDefaultZone();
    this.created = Instant.now(clock.withZone(ZoneId.of(TIME_ZONE)));
    this.updated = Instant.now(clock.withZone(ZoneId.of(TIME_ZONE)));
  }

  @PreUpdate
  protected void onUpdate() {
    var clock = Clock.systemDefaultZone();
    this.updated = Instant.now(clock.withZone(ZoneId.of(TIME_ZONE)));
  }
}
