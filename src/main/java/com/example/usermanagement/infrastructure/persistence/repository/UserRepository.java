
package com.example.usermanagement.infrastructure.persistence.repository;

import com.example.usermanagement.infrastructure.persistence.entity.UserJpa;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<UserJpa, String> {
    UserJpa save(UserJpa user);

    UserJpa findByUsernameAndPassword(String username, String password);
}
