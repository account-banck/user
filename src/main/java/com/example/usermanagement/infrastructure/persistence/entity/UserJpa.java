
package com.example.usermanagement.infrastructure.persistence.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@Table(name = "\"user\"") // Modification ici pour éviter l'erreur de mot réservé
@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class UserJpa extends BaseJpa {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    @Column(unique = true, length = 36)
    private String id;

    @Column
    private String username;
    @Column
    private String password;
}
