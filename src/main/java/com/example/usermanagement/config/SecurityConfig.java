package com.example.usermanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .oauth2Login(oauth2 -> oauth2
                        .authorizationEndpoint(authorization -> authorization
                                // Configurer l'Authorization Endpoint
                                .baseUri("/oauth2/authorize")
                                .authorizationRequestRepository(myCustomAuthorizationRequestRepository())
                        )
                        .tokenEndpoint(token -> token
                                // Configurer le Token Endpoint
                                .accessTokenResponseClient(myCustomAccessTokenResponseClient())
                        )
                        .userInfoEndpoint(userInfo -> userInfo
                                // Configurer l'UserInfo Endpoint
                                .userService(myCustomOAuth2UserService())
                        )
                );
        return http.build();
    }

    @Bean
    public MyCustomAuthorizationRequestRepository myCustomAuthorizationRequestRepository() {
        return new MyCustomAuthorizationRequestRepository();
    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository(googleClientRegistration());
    }

    private ClientRegistration googleClientRegistration() {
        return ClientRegistration.withRegistrationId("google")
                .clientId("your-client-id")
                .clientSecret("your-client-secret")
                .scope("openid", "profile", "email")
                .authorizationUri("https://accounts.google.com/o/oauth2/auth")
                .tokenUri("https://oauth2.googleapis.com/token")
                .userInfoUri("https://openidconnect.googleapis.com/v1/userinfo")
                .userNameAttributeName("sub")
                .clientName("Google")
                .redirectUri("{baseUrl}/login/oauth2/code/{registrationId}")
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .build();
    }

    @Bean
    public MyCustomAccessTokenResponseClient myCustomAccessTokenResponseClient() {
        return new MyCustomAccessTokenResponseClient();
    }

    @Bean
    public MyCustomOAuth2UserService myCustomOAuth2UserService() {
        return new MyCustomOAuth2UserService();
    }
}



