package com.example.usermanagement.config;

import org.springframework.http.*;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class MyCustomAccessTokenResponseClient implements OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> {
    @Override
    public OAuth2AccessTokenResponse getTokenResponse(OAuth2AuthorizationCodeGrantRequest authorizationGrantRequest) {
        RestTemplate restTemplate = new RestTemplate();

        String url = "https://your-auth-server.com/oauth/token"; // URL du serveur d'autorisation
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("client-id", "client-secret"); // Remplacez par vos credentials

        HttpEntity<String> entity = new HttpEntity<>("code=" + authorizationGrantRequest.getAuthorizationExchange().getAuthorizationResponse().getCode() + "&grant_type=authorization_code&redirect_uri=your-redirect-uri", headers);

        ResponseEntity<OAuth2AccessTokenResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, OAuth2AccessTokenResponse.class);
        return responseEntity.getBody();
    }
}
