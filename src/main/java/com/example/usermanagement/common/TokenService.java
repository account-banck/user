package com.example.usermanagement.common;


import com.example.usermanagement.domain.model.CustomTokenResponse;
import com.example.usermanagement.domain.model.User;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final JwtEncoder jwtEncoder;

    public CustomTokenResponse tokenResponse(User user) {
        Jwt accessToken = generateAccessToken(user);
        Jwt refreshToken = generateRefreshToken(user);
        Jwt idToken = generateIdToken(user);

        return CustomTokenResponse.builder()
                .accessToken(accessToken.getTokenValue())
                .bearerAccessToken(new BearerAccessToken(accessToken.getTokenValue(), calculateRemainingTime(accessToken), null))
                .refreshToken(new RefreshToken(refreshToken.getTokenValue()))
                .idToken(idToken.getTokenValue()).build();

    }

    private long calculateRemainingTime(Jwt jwt) {
        long exp = jwt.getExpiresAt().getEpochSecond();
        return exp - Instant.now().getEpochSecond();
    }

    // Méthodes pour générer les tokens
    private Jwt generateAccessToken(User user) {
        return generateToken(user, 3600); // Access token with 1 hour validity
    }

    private Jwt generateRefreshToken(User user) {
        return generateToken(user, 7200); // Refresh token with 2 hours validity
    }

    private Jwt generateIdToken(User user) {
        return generateToken(user, 3600); // ID token with 1 hour validity
    }

    private Jwt generateToken(User user, long validityInSeconds) {
        long expiryTime = Instant.now().getEpochSecond() + validityInSeconds;
        // Préparation du Consumer pour configurer les claims
        Consumer<Map<String, Object>> claimsConsumer = claims -> {
            claims.put("iss", "http://your-issuer-url.com");
            claims.put("username", user.getUsername());
            // Décommentez et implémentez la récupération des rôles si l'utilisateur en possède
            // claims.put("roles", user.getRoles());
            claims.put("exp", expiryTime);
        };

// Utilisation du Consumer dans un contexte approprié
        Map<String, Object> claims = new HashMap<>();
        claimsConsumer.accept(claims);

        JwtClaimsSet claimsSet = JwtClaimsSet.builder()
                .issuer("http://your-issuer-url.com")
                .subject(user.getUsername())
                .expiresAt(Instant.ofEpochSecond(expiryTime))
                .claims(claimsConsumer)

                .build();

        return jwtEncoder.encode(JwtEncoderParameters.from(claimsSet));
    }

    public String generateToken(User user) {
        var jwt = generateAccessToken(user);
        return jwt.getTokenValue();
    }
}
