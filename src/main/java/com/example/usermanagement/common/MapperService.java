package com.example.usermanagement.common;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@RequiredArgsConstructor
@Component
public class MapperService {
    private static final String CLASS_SUFFIX = "Mapper";
    private final ApplicationContext applicationContext;

    public <T> T getByType(Class<T> clazz) {
        if (!clazz.getSimpleName().endsWith(CLASS_SUFFIX)) {
            throw new UnsupportedOperationException(
                    MessageFormat.format("{0} is not a Mapstruct mapper", clazz));
        }
        return applicationContext.getBean(clazz);
    }
}
