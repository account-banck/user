
package com.example.usermanagement.application.service;

import com.example.usermanagement.common.MapperService;
import com.example.usermanagement.common.TokenService;
import com.example.usermanagement.domain.mapper.generics.UserJpaMapper;
import com.example.usermanagement.domain.model.CustomTokenResponse;
import com.example.usermanagement.domain.model.User;
import com.example.usermanagement.domain.port.UserService;
import com.example.usermanagement.infrastructure.persistence.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final MapperService mapperService;
    private final TokenService tokenService;

    @Override
    public User registerUser(User user) {
        var mapper = mapperService.getByType(UserJpaMapper.class);
        var userJpa = userRepository.save(mapper.toJpa(user));
        return mapper.toDomain(userJpa);
    }

    @Override
    public CustomTokenResponse loginUser(String username, String password) {

        var user = userRepository.findByUsernameAndPassword(username, password);
        var mapper = mapperService.getByType(UserJpaMapper.class);
        return tokenService.tokenResponse(mapper.toDomain(user));
    }

    @Override
    public void logoutUser(String userId) {
        // Logique de déconnexion
    }
}
