package com.example.usermanagement.domain.port;

import com.example.usermanagement.application.service.UserServiceImpl;
import com.example.usermanagement.domain.model.CustomTokenResponse;
import com.example.usermanagement.domain.model.User;
import com.example.usermanagement.infrastructure.persistence.entity.UserJpa;
import com.example.usermanagement.infrastructure.persistence.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UserServiceTest {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserRepository userRepository;


    private UserJpa user;

    @BeforeEach
    void setUp() {
        // Configurer un utilisateur de test
        user = new UserJpa();
        user.setUsername("testUser");
        user.setPassword("password123");
        user = userRepository.save(user); // Enregistre l'utilisateur dans la base de données H2
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll(); // Nettoyer la base de données après chaque test
    }

    @Test
    void testRegisterUser() {
        User newUser = new User();
        newUser.setUsername("newUser");
        newUser.setPassword("newPassword");

        User registeredUser = userService.registerUser(newUser);

        assertThat(registeredUser).isNotNull();
        assertThat(registeredUser.getUsername()).isEqualTo("newUser");
        assertThat(userRepository.existsById(registeredUser.getId())).isTrue();
    }

    @Test
    void testLoginUser() {
        CustomTokenResponse loggedInUser = userService.loginUser(user.getUsername(), user.getPassword());
        assertThat(loggedInUser).isNotNull();

    }

    @Test
    void testLogoutUser() {
        userService.logoutUser(user.getId());
        // Assurer que la logique de déconnexion est testée, dépend des détails de votre implémentation
    }
}
